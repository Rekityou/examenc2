import express from 'express';
export const router = express.Router();

router.get("/", (req, res) => {

    const params = {
        numero: req.query.numero, 
        nombre: req.query.nombre, 
        domicilio: req.query.domicilio, 
        nivel: req.query.nivel, 
        pagoHoraBase: req.query.pagoHoraBase, 
        horas: req.query.horas, 
        numHijos: req.query.numHijos,
        isPost: false,
    };

    res.render("index", params);

});

router.post("/", (req, res) => {

    const { 
        numero, 
        nombre, 
        domicilio, 
        nivel, 
        pagoHoraBase, 
        horas, 
        numHijos 
    } = req.body;
  
    const nivelNumerico = parseInt(nivel);
    const pagoHoraBaseNumerico = parseFloat(pagoHoraBase);
    const horasNumericas = parseFloat(horas);
    const numHijosNumerico = parseInt(numHijos);
  
    const pagoPorHoras = calcularPago(parseInt(nivel), parseFloat(pagoHoraBase), parseFloat(horas));
    const bono = calcularBono(parseInt(numHijos), pagoPorHoras);
    const impuesto = calcularImpuesto(pagoPorHoras);
    const totalAPagar = calcularTotalAPagar(pagoPorHoras, bono, impuesto);
  
    const params = {
        numero,
        nombre,
        domicilio,
        nivel: nivelNumerico,
        pagoHoraBase: pagoHoraBaseNumerico,
        horas: horasNumericas,
        numHijos: numHijosNumerico,
        pagoPorHoras,
        bono,
        impuesto,
        totalAPagar,
        isPost: true
    };

    res.render("index", params);
});  

function calcularPago(nivel, pagoHoraBase, horas) {
    const incrementos = [0.3, 0.5, 1.0];
    const incremento = incrementos[nivel - 1];
    return pagoHoraBase * (1 + incremento) * horas;
}

function calcularImpuesto(pagoPorHoras) {
    return pagoPorHoras * 0.16;
}

function calcularBono(numHijos, pagoPorHoras) {
    let porcentajeBono = 0;
    numHijos = parseInt(numHijos);

    if (numHijos >= 1 && numHijos <= 2) {
        porcentajeBono = 0.05;
    } else if (numHijos >= 3 && numHijos <= 5) {
        porcentajeBono = 0.10;
    } else if (numHijos > 5) {
        porcentajeBono = 0.20;
    }

    return pagoPorHoras * porcentajeBono;
}

function calcularTotalAPagar(pagoPorHoras, bono, impuesto) {
    return pagoPorHoras + bono - impuesto;
}

//Los calculos funcionan acorde al ejemplo del profesor
    
export default { router };